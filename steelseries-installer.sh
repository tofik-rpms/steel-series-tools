#!/bin/bash

XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"
WINEPREFIX="${XDG_DATA_HOME}/steelseries"

STEELSERIES_DOWNLOAD_URL="https://steelseries.com/gg/downloads/gg/latest/windows"
INSTALLER_PATH=/tmp/steelseries.exe
export WINEPREFIX

curl -L -o "${INSTALLER_PATH}" "${STEELSERIES_DOWNLOAD_URL}"

wine reg add HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus /v Enable\ SDL /t Reg_Dword /d 0 /f
wine "${INSTALLER_PATH}"

