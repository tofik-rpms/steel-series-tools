Name:           steelseries-installer
Version:        0.1.0
Release:        %{autorelease}
Summary:        Installer script and udev rules for SteelSeries Engine
BuildArch:      noarch

License:        GPLv3
URL:            https://gitlab.com/tofik-rpms/steel-series-tools
Source0:        steelseries-installer.sh
Source1:        udev.rules

BuildRequires:  systemd-rpm-macros
Requires:       wine
Requires:       wine-fonts

%description
%{summary}

%prep


%build


%install
install -D -m755 %{SOURCE0} %{buildroot}%{_bindir}/steelseries-installer
install -D -m644 %{SOURCE1} %{buildroot}%{_udevrulesdir}/60-steelseries.rules


%check

%files
%license
%{_bindir}/steelseries-installer
%{_udevrulesdir}/60-steelseries.rules

%changelog
%{autochangelog}

